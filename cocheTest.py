from coche import Coche
import unittest

class CocheTestCase(unittest.TestCase):
    def test_aceleracion(self):
        coche01 = Coche("rojo", "seat", "ibiza", "1234abc", 20)
        aceleracion = coche01.aceleracion()
        self.assertEqual (aceleracion, 40)

    def test_freno(self):
        coche02 = Coche("rojo", "seat", "ibiza", "1234abc", 20)
        freno = coche02.freno()
        self.assertEqual(freno, 10)
      
if __name__ == '__main__':
    unittest.main()